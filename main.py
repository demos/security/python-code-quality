
import unittest
import numpy as np

class RomanNumeralConverter(object):
    def __init__(self, roman_num):
        self.roman_num = roman_num
        self.digit_map = {"I":1, "V":5, "X":10, "L":50, "C":100}
        self.text_map = {"I":"One", "V":"Five", "X":"Ten", "L":"Fifty", "C":"One Hundred"}

    def convert_to_decimal(self):
        value = 0
        for char in self.roman_num:
            value += self.digit_map[char]

        return value

    def express_in_words(self):
        value_text = "text"
        for char in self.roman_num:
            value_text = self.text_map[char]

        return value_text

def find_item_costs(file_path):
    """
    #Args: Path File containing the costs

    #Output: item costs

    """
    with open(file_path) as file:
        item_costs = file.read().split('\n')
    item_costs = np.array(item_costs).astype(int)
    total_price = (item_costs[item_costs < 25]).sum() * 1.08

    return total_price


if __name__ == "__main__":
    ITEM_COSTS = find_item_costs('gift_costs.txt')
    print(ITEM_COSTS)

